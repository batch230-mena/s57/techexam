let collection = [];

// Write the queue functions below.

module.exports.print = () => {
	return collection;
};

module.exports.enqueue = (data) => {
	let length = collection.length;
	collection[length] = data;
	return collection;
};

module.exports.dequeue = () => {
	collection.splice(0,1);
	return collection;
};

module.exports.front = () => {
	return collection[0];
};

module.exports.size = () => {
	return collection.length;
};

module.exports.isEmpty = () => {
	if(collection.length === 0){
		return true;
	}
	else{
		return false;
	}
};